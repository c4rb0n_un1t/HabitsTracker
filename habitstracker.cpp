#include "habitstracker.h"

HabitsTracker::HabitsTracker(QObject* parent,
        ReferenceInstancePtr<IHabitsTrackerDataExtention> dataExtention,
        ReferenceInstancePtr<IUserTaskRepeatDataExtention> userTasks) :
	QObject(parent),
	m_dataExtention(dataExtention),
	m_userTasks(userTasks)
{
	m_currentDate = QDateTime::currentDateTime();
	connect(&m_dayChangeTimer, &QTimer::timeout, this, [=]() {
		for(auto& habbit : m_habits)
		{
			qobject_cast<Habbit*>(habbit)->onDayChanged();
		}
		setupTimer();
	});
	setupTimer();
}

void HabitsTracker::setupTimer()
{
	auto nextDayStart = m_currentDate.addDays(1);
	m_dayChangeTimer.start(m_currentDate.msecsTo(nextDayStart));
}

void HabitsTracker::init()
{
	connect(m_dataExtention.reference()->object(), SIGNAL(modelChanged()), SLOT(updateModel()));
	auto model = m_dataExtention->getModel();
	if(!model.isNull())
		updateModel();
}

int HabitsTracker::totalStreak()
{
	return m_totalStreak;
}

QList<QObject*> HabitsTracker::habits()
{
	return m_habits.values();
}

QDate HabitsTracker::currentDate()
{
	return m_currentDate.date();
}

void HabitsTracker::addHabit(QString name)
{
	auto taskData = m_userTasks->getModel()->getDefaultItem();
	taskData[INTERFACE(IUserTaskDataExtention)]["name"] = name;
	auto containerItemId = m_userTasks->getModel()->appendItem(taskData);
	auto containerIndex = m_userTasks->getModel()->getItemIndex(containerItemId);

	taskData[INTERFACE(IUserTaskDateDataExtention)]["startDate"] = QDateTime::currentDateTime();
	taskData[INTERFACE(IUserTaskDateDataExtention)]["dueDate"] = QDateTime::currentDateTime().addDays(1);
	taskData[INTERFACE(IUserTaskRepeatDataExtention)]["repeatType"] = RepeatType::DAILY;
	auto taskItemId = m_userTasks->getModel()->addItem(taskData, 0, containerIndex);

	m_taskToHabbit[taskItemId] = name;
	m_habbitToTask[name] = taskItemId;

	auto habbitData = m_dataExtention->getModel()->getDefaultItem();
	habbitData[INTERFACE(IHabitsTrackerDataExtention)]["name"] = name;
	habbitData[INTERFACE(IHabitsTrackerDataExtention)]["streak"] = 0;
	habbitData[INTERFACE(IHabitsTrackerDataExtention)]["lastTaskId"] = taskItemId;
	habbitData[INTERFACE(IHabitsTrackerDataExtention)]["containerTaskId"] = containerItemId;
	m_dataExtention->getModel()->appendItem(habbitData);
}

void HabitsTracker::updateModel()
{
	auto model = m_dataExtention->getModel();
	connect(model.data(), SIGNAL(modelChanged()), SLOT(updateData()));
	connect(m_userTasks.reference()->object(), SIGNAL(spawnedNewTask(int, int)), SLOT(trackSpawnedTask(int, int)));
	updateData();
}

void HabitsTracker::updateData()
{
	auto model = m_dataExtention->getModel();
	auto itemIds = model->getItemIds();
	m_totalStreak = 0;

	for(auto& id : itemIds)
	{
		auto data = model->getItem(id);
		auto habitsData = data[INTERFACE(IHabitsTrackerDataExtention)];
		auto name = habitsData["name"].toString();
		auto streak = habitsData["streak"].toInt();
		auto lastTaskId = habitsData["lastTaskId"].toInt();
		auto containerTaskId = habitsData["containerTaskId"].toInt();
		auto lastCompletedDate = habitsData["lastCompletedDate"].toDate();

		m_totalStreak += streak;
		m_taskToHabbit[lastTaskId] = name;
		m_habbitToTask[name] = lastTaskId;
		auto& object = m_habits[name];
		if(!object)
		{
			auto habbit = new Habbit(this, name, streak, id, containerTaskId, lastCompletedDate);
			connect(habbit, &Habbit::tryToComplete, this, &HabitsTracker::tryToComplete);
			connect(habbit, &Habbit::removeHabbit, this, &HabitsTracker::removeHabbit);
			object = habbit;
		}
	}
	emit habitsChanged();
	emit totalStreakChanged();
}

void HabitsTracker::tryToComplete(QString name)
{
	auto lastTaskId = m_habbitToTask[name];
	auto taskData = m_userTasks->getModel()->getItem(lastTaskId);
	taskData[INTERFACE(IUserTaskDataExtention)]["isDone"] = true;
	m_userTasks->getModel()->updateItem(lastTaskId, taskData);
}

void HabitsTracker::removeHabbit(QString name)
{
	auto habbit = qobject_cast<Habbit*>(m_habits[name]);
	m_dataExtention->getModel()->removeItem(habbit->habbitId());
	auto lastTaskId = m_habbitToTask[name];
	m_taskToHabbit.remove(lastTaskId);
	m_habbitToTask.remove(name);
	m_habits.remove(habbit->name());

	delete habbit;
	emit habitsChanged();
}

void HabitsTracker::trackSpawnedTask(int completedTaskId, int spawnedTaskId)
{
	auto iter = m_taskToHabbit.find(completedTaskId);
	if(iter != m_taskToHabbit.end())
	{
		auto name = iter.value();

		auto habbit = qobject_cast<Habbit*>(m_habits[name]);
		auto habbitId = habbit->habbitId();
		auto habbitData = m_dataExtention->getModel()->getItem(habbitId);
		auto streak = habbitData[INTERFACE(IHabitsTrackerDataExtention)]["streak"].toInt() + 1;
		habbitData[INTERFACE(IHabitsTrackerDataExtention)]["streak"] = streak;
		habbitData[INTERFACE(IHabitsTrackerDataExtention)]["lastTaskId"] = spawnedTaskId;
		habbitData[INTERFACE(IHabitsTrackerDataExtention)]["lastCompletedDate"] = QDate::currentDate();
		m_dataExtention->getModel()->updateItem(habbitId, habbitData);

		habbit->taskCompleted(streak);
		m_taskToHabbit[spawnedTaskId] = name;
		m_habbitToTask[name] = spawnedTaskId;
	}
}
